import ReactDOM from 'react-dom/client';

import './index.css';
import config from './config'

import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';
import { configure } from '@monterosa-sdk/core';

configure(config);

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
    <App />
);
