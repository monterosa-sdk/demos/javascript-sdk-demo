import { useEffect, useRef } from 'react';
import { dismount, embed, getExperience, onMessage } from '@monterosa-sdk/launcher-kit';

function Experience() {
  const experienceContainer = useRef(null);

  useEffect(() => {
    const container = experienceContainer.current;
    if (!container) {
      return;
    }

    console.log("Embedding");
    
    const experience = getExperience({
      autoresizesHeight: true,
    });

    const unsubscribe = onMessage(experience, (message) => {
      console.log("Message received:", message);
    })

    embed(experience, container);
    
    return () => {
      dismount(container);
      unsubscribe();
    }
  }, []);

  return (
    <>
        <div ref={experienceContainer} style={{
          'maxWidth': 600
        }}/>
    </>
  );
}

export default Experience;
