import Alert from 'react-bootstrap/Alert';

import { Body } from './styles';
import Experience from './Experience';

function App() {
  return (
    <>
      <Body>
        <h1>Monterosa / Interaction SDK - Demo Page</h1>
        <h2>Embedding Experiences</h2>

        <Alert variant="info">Monterosa / Interaction SDK v2 is currently in beta with the public release due in autumn 2022. You can learn more about it <a href="https://products.monterosa.co/mic/experiences/integration-guide/embedding-experiences-using-the-sdk">here</a>.</Alert>

        <p>The SDK can be used to easily embed an <a href="https://products.monterosa.co/mic/experiences"><i>Experience</i></a> into your web or native app. This provides preloading and resizing capabilities which save you integration effort and make for a smooth user experience.</p>
        <p>You can load any of the ready-made Experiences from the <a href="https://products.monterosa.co/mic/experiences/experience-library">Experience Library</a> or those that have been developed specifically by/for you and made available in the platform.</p>
        <p>Using the SDK to embed Experiences is not mandatory, you can also deploy Experiences using <a href="https://products.monterosa.co/mic/experiences/integration-guide">custom code</a> to manage loading and sizing.</p>

        <hr />

        <h2>Embed Example</h2>

        <p>The following Experience has been embedded using the SDK. Resize the window to observe responsiveness.</p>

        <p>You can find the source code for this web app <a href="https://gitlab.com/monterosa-sdk/demos/javascript-sdk-demo">here</a>.</p>

        <Experience />
      </Body>
    </>
  );
}

export default App;
