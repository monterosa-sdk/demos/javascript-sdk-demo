import styled from "styled-components";
import Container from "react-bootstrap/Container";

export const Header = styled.header`
  display: flex;
  align-items: center;
  height: 60px;
  border-bottom: 1px solid rgba(211, 220, 228, 1);
  box-shadow: 0px 4px 10px rgb(0 0 0 / 5%);
  background-color: #fff;
`;

export const Body = styled(Container)`
  padding-top: 80px;
  padding-bottom: 80px;
`;

export const Footer = styled.div`
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 60px;
  display: flex;
  align-items: center;
  /* border-top: 1px solid rgba(211, 220, 228, 1); */
  /* box-shadow: 0px -4px 10px rgb(0 0 0 / 5%); */
  background-color: #f5f5f5;
`;
